import React from "react";
import "./Tile.css";

const Tile = ({ value, onClick, isZero = false }) => {
  const getClassName = (btn) => {
    const styles = {
      AC: "light-gray",
      "%": "light-gray",
      "+/-": "light-gray",
      "/": "orange",
      X: "orange",
      "+": "orange",
      "-": "orange",
      0: "zero",
    };

    return styles[btn];
  };

  return (
    <div
      className={`${getClassName(value)} btn`}
      onClick={() => onClick(value)}
    >
      {value === "AC" ? `${!isZero ? "C" : value}` : value}
    </div>
  );
};

export default Tile;

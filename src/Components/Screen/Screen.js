import React from "react";
import { CalculateContext } from "../../context/CalculateContext";
import "./Screen.css";

const Screen = (props) => {
  const { calculate } = React.useContext(CalculateContext);

  return (
    <div className="screen">
      {calculate.number ? calculate.number : calculate.result}
    </div>
  );
};

export default Screen;

import React from "react";
import "./Container.css";

//* Components
import Screen from "../Screen/Screen";
import Tile from "../Tile/Tile";

//* Context
import { CalculateContext } from "../../context/CalculateContext";

//* Constants
import { btns } from "../constants";

const Container = (props) => {
  const [isZero, setIsZero] = React.useState(false);
  const { setCalculate, calculate } = React.useContext(CalculateContext);

  React.useEffect(() => {
    if (calculate.number == 0 && calculate.result == 0) {
      setIsZero(true);
    } else {
      setIsZero(false);
    }
  }, [calculate]);

  //* On Dot Click
  const dotClick = (value) => {
    setCalculate({
      ...calculate,
      number: !calculate.number.toString().includes(".")
        ? calculate.number + value
        : calculate.num,
    });
  };

  //* On AC Click
  const ACClick = () => {
    setCalculate({
      number: 0,
      result: 0,
      sign: "",
    });
  };

  //* On Number Click
  const numberClick = (value) => {
    const numberStr = value.toString();

    let _number;

    //* if first click 0 (zero)
    if (numberStr === "0" && calculate.number === 0) _number = "0";
    else _number = Number(calculate.number + numberStr);

    setCalculate({
      ...calculate,
      number: _number,
    });
  };

  //* On math sign click
  const signClick = (value) => {
    setCalculate({
      result:
        !calculate.result && calculate.number
          ? calculate.number
          : calculate.result,
      sign: value,
      number: 0,
    });
  };

  //* On Equal click
  const equalsClick = React.useCallback(() => {
    if (calculate.result && calculate.number) {
      const math = (x, y, sign) => {
        const res = {
          X: (x, y) => x * y,
          "+": (x, y) => x + y,
          "-": (x, y) => x - y,
          "/": (x, y) => x / y,
        };

        return res[sign](x, y);
      };

      setCalculate({
        result: math(calculate.result, calculate.number, calculate.sign),
        sign: "",
        number: 0,
      });
    }
  }, [calculate]);

  //* On percent Click
  const percentClick = () => {
    setCalculate({
      sign: "",
      number: calculate.number / 100,
      result: calculate.result / 100,
    });
  };

  //* On Plus/Minus Click
  const plusMinusClick = () => {
    setCalculate({
      sign: "",
      number: calculate.number ? calculate.number * -1 : 0,
      result: calculate.result ? calculate.result * -1 : 0,
    });
  };

  //* handle on click
  const handleOnClick = (value) => {
    const res = {
      "+": signClick,
      "-": signClick,
      X: signClick,
      "/": signClick,
      "=": equalsClick,
      AC: ACClick,
      ".": dotClick,
      "%": percentClick,
      "+/-": plusMinusClick,
    };

    if (res[value]) res[value](value);
    else numberClick(value);
  };

  return (
    <div className="container">
      <div className="wrapper">
        <Screen />

        {btns.map((value, index) => (
          <Tile
            value={value}
            key={index}
            onClick={handleOnClick}
            isZero={isZero}
          />
        ))}
      </div>
    </div>
  );
};

export default Container;

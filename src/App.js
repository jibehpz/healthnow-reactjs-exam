import "./App.css";
import React from "react";

import CalculatorContainer from "./Components/Container/Container";
import CalculateProvider from "./context/CalculateContext";

const App = () => {
  return (
    <div>
      <CalculateProvider>
        <CalculatorContainer />
      </CalculateProvider>
    </div>
  );
};

export default App;

import React from "react";

export const CalculateContext = React.createContext();

const CalculateProvider = ({ children }) => {
  const [calculate, setCalculate] = React.useState({
    number: 0,
    result: 0,
    sign: "",
  });

  return (
    <CalculateContext.Provider
      value={{
        calculate,
        setCalculate,
      }}
    >
      {children}
    </CalculateContext.Provider>
  );
};

export default CalculateProvider;
